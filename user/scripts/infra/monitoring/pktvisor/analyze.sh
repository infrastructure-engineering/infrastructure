#!/bin/bash

# start agent

docker run --net=host -d ns1labs/pktvisor pktvisord wlp3s0 # your interfaces

# show monitoring program

docker run -it --rm --net=host ns1labs/pktvisor pktvisor-cli

