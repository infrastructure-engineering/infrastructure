#!/bin/bash

SCRIPT_DIR="$( cd -- "$( dirname -- "${BASH_SOURCE[0]:-$0}"; )" &> /dev/null && pwd 2> /dev/null; )"

source $SCRIPT_DIR/machines.conf

for m in  $machine_list; do
    echo "MaChine : $m"
    ssh $m "$@"
done
