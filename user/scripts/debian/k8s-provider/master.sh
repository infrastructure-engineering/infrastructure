#!/bin/bash

################################# Install Helm 3 #######################################################

curl -fsSL -o get_helm.sh https://raw.githubusercontent.com/helm/helm/master/scripts/get-helm-3
chmod +x get_helm.sh
./get_helm.sh

################################# k8s master node setup ################################################

echo "add .bashrc last line 'export KUBECONFIG=/etc/kubernetes/admin.conf'"
export KUBECONFIG=/etc/kubernetes/admin.conf

rm /etc/containerd/config.toml

########################################################################################################


sudo kubeadm init --pod-network-cidr=192.168.0.0/16
kubeadm config images pull
kubeadm token create --print-join-command > ~/token-k8s.txt


curl https://projectcalico.docs.tigera.io/manifests/calico.yaml -O
kubectl apply -f calico.yaml
